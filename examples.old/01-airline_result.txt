Experiment all_results for
 datafile = ../data/tsdlr/01-airline.mat

 Running experiment:
description = An example experiment,
data_dir = ../data/tsdlr/,
max_depth = 2,
random_order = False,
k = 1,
debug = False,
local_computation = True,
n_rand = 2,
sd = 2,
jitter_sd = 0.1,
max_jobs = 500,
verbose = True,
make_predictions = False,
skip_complete = False,
results_dir = ../examples/,
iters = 10,
base_kernels = SE,Per,Lin,
additive_form = False,
mean = ff.MeanZero(),
kernel = ff.NoiseKernel(),
lik = ff.LikGauss(sf=-np.Inf),
verbose_results = False,
random_seed = 0,
period_heuristic = 10,
max_period_heuristic = 5,
subset = False,
subset_size = 250,
full_iters = 0,
bundle_size = 1,
search_operators = None,
score = BIC,
period_heuristic_type = both,
stopping_criteria = [],
improvement_tolerance = 0.1,
 


%%%%% Level 0 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[NoiseKernel(sf=0.508911595282), SumKernel(operands=[ConstKernel(sf=4.51544563377), LinearKernel(dimension=0, location=1950.35267524, sf=3.31551170879)])]), likelihood=LikGauss(sf=-inf), nll=1005.76333844, ndata=144)

%%%%% Level 1 %%%%%

GPModel(mean=MeanZero(), kernel=ProductKernel(operands=[SumKernel(operands=[ConstKernel(sf=3.60512485788), LinearKernel(dimension=0, location=1950.51586871, sf=2.74268452668)]), SumKernel(operands=[NoiseKernel(sf=-0.758005629098), SqExpKernel(dimension=0, lengthscale=2.71739823689, sf=1.51393325405)])]), likelihood=LikGauss(sf=-inf), nll=727.676565524, ndata=144)
