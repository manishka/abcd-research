# An example experiment that runs quickly.

Experiment(description='An 2014-01-16-GPSS-full experiment',
		data_dir='../data/tsdlr-renamed/05-temperature.mat',
		results_dir='../mak-results/2016-05-13-GPSS-full/',
		max_depth=10,		  # How deep to run the search.
		k=1,				  # Keep the k best kernels at every iteration.  1 => greedy search.
		n_rand=9,		     # Number of random restarts.
		local_computation = True,
		sd=2,
		jitter_sd=0.1,
		max_jobs = 400,
		iters=250,		     # How long to optimize hyperparameters for.
		base_kernels='SE,Per,Lin,Const,Noise',
		verbose=True,
		random_seed = 3,
		period_heuristic = 3,
		subset = True,
		full_iters = 10,
		bundle_size = 5,
		search_operators = [('A', ('+', 'A', 'B'), {'A': 'kernel', 'B': 'base'}), ('A', ('*', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', ('*-const', 'A', 'B'), {'A': 'kernel', 'B': 'base-not-const'}), ('A', 'B', {'A': 'kernel', 'B': 'base'}), ('A', ('CP', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('CW', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('B', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('BL', 'd', 'A'), {'A': 'kernel', 'd': 'dimension'}), ('A', ('None',), {'A': 'kernel'})],
		period_heuristic_type = 'min',
		make_predictions=False,
		skip_complete=True)
