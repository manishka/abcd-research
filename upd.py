import subprocess
import time
def command(cmd):
     output = ""
     try:
             process = subprocess.Popen(cmd,stdout=subprocess.PIPE, shell=True)
             output = process.communicate()[0].strip()
     except Exception as e:
             print e
     return output
while True:
	output_text = command('squeue -o "%%i  %%j %%T %%M %%l " -u $USER')	 
	for line in output_text.split('\n'):
		if 'PENDING' in line and not '25:00' in line.split()[4]:
			out = command('scontrol update TimeLimit=25:00 JobId=%s' % line.split()[0])
			print out, "updated %s" % line.split()[0]
	time.sleep(.5)
